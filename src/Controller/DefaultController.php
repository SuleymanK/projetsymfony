<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/default", name="default")
     */
    public function index(){

        $number = random_int(0, 100);

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'nombre' => $number,
        ]);
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class Exercice1Controller extends AbstractController
{
    /**
     * @Route("/exercice1", name="exercice1")
     */
    public function index(){

        $number1 = random_int(0, 100);
        $number2 = random_int(0, 100);

        return $this->render('exercice1/index.html.twig', [
            'controller_name' => 'Exercice1Controller',
            'nombre1' => $number1,
            'nombre2' => $number2,
        ]);
    }
}

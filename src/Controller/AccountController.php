<?php

namespace App\Controller;


use App\Entity\Account;
use App\Form\AccountCreateType;
use App\Form\BeneficiaireCreateType;
use App\Repository\AccountRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\TransactionRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AccountController extends AbstractController
{
    /**
     * @Route("/account/create", name="create_account")
     * @Route("/account/{id}/edit", name="edit_account")
     */
    public function form(Account $account = null, Request $request, EntityManagerInterface $em, AuthenticationUtils $authenticationUtils){

        if(!$account){
            $account = new Account();
        }

        $form = $this->createForm(AccountCreateType::class, $account);
        $lastUsername = $authenticationUtils->getLastUsername();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $account = $form->getData();
            $em->persist($account);
            $em->flush();

            return $this->redirectToRoute('list_account');
        }

        return $this->render('account/create.html.twig',[
            'form' => $form->createView(),
            'editMode' => $account->getId() !== null,
            'nomUser' => $lastUsername,
        ]);
    }

    /**
     * @Route("/account", name="list_account")
     */
    public function list(AccountRepository $repo,  AuthenticationUtils $authenticationUtils){

        $accounts = $repo->findAll();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render("account/index.html.twig", [
            'accounts' => $accounts,
            'nomUser' => $lastUsername,
        ]);
    }

    /**
     * @Route("/account/delete/{id}", name="delete_account")
     */
    public function delete(Account $account, EntityManagerInterface $em){

        $em->remove($account);
        $em->flush();

        return new Response("Ok delete");
    }

    /**
     * @Route("/account/{id}/transaction", name="transaction_account")
     */
    public function transaction(Account $account, AuthenticationUtils $authenticationUtils){

        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render("account/liste_transaction.html.twig", [
            'account' => $account,
            'nomUser' => $lastUsername,

        ]);
    }

    /**
     * @Route("/account/{id}/beneficiare/add", name="add_beneficiaire")
     */
    public function addBeneficiaire(Account $account, Request $request, EntityManagerInterface $em, AuthenticationUtils $authenticationUtils){

        $lastUsername = $authenticationUtils->getLastUsername();
        $form = $this->createForm(BeneficiaireCreateType::class, $account);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $benef = $form->get("beneficiaire")->getData();
            foreach($benef as $beneficiary){
                $account->addBeneficiaire($beneficiary);
            }
            $em->flush();

            return $this->redirectToRoute('list_account');
        }

        return $this->render("account/addBeneficiaire.html.twig", [
            'form' => $form->createView(),
            'nomUser' => $lastUsername,
        ]);
    }

    /**
     * @Route("/account/{id}/beneficiaire/del/{id_benef}", name="del_beneficiaire")
     * @Entity("Account", expr="repository.find(id_benef)")
     */
    public function delBeneficiaire(Account $account, Account $id_benef, Request $request, EntityManagerInterface $em, AuthenticationUtils $authenticationUtils){
            $account->removeBeneficiaire($id_benef);

            $em->flush();

            return $this->redirectToRoute("transaction_account", array(
                'id' => $account->getId(),
            ));
    }
}

<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountRepository")
 */
class Account
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="integer")
     */
    private $balance;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="departureAccount", orphanRemoval=true)
     */
    private $departureTransactions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="arrivalAccount", orphanRemoval=true)
     */
    private $arrivalTransactions;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Account")
     * 
     */
    private $beneficiaire;

    public function __construct()
    {
        $this->departureTransactions = new ArrayCollection();
        $this->arrivalTransactions = new ArrayCollection();
        $this->beneficiaire = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getBalance(): ?int
    {
        return $this->balance;
    }

    public function addToBalance(int $amount): ?int
    {
        return $this->balance += $amount;
    }

    public function subToBalance(int $amount): ?int
    {
        return $this->balance -= $amount;
    }

    public function setBalance(int $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getDepartureTransactions(): Collection
    {
        return $this->departureTransactions;
    }

    public function addDepartureTransaction(Transaction $transaction): self
    {
        if (!$this->departureTransactions->contains($transaction)) {
            $this->departureTransactions[] = $transaction;
            $transaction->setAccount($this);
        }

        return $this;
    }

    public function removeDepartureTransaction(Transaction $transaction): self
    {
        if ($this->departureTransactions->contains($transaction)) {
            $this->departureTransactions->removeElement($transaction);
            // set the owning side to null (unless already changed)
            if ($transaction->getAccount() === $this) {
                $transaction->setAccount(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getArrivalTransactions(): Collection
    {
        return $this->arrivalTransactions;
    }

    public function addArrivalTransaction(Transaction $arrivalTransaction): self
    {
        if (!$this->arrivalTransactions->contains($arrivalTransaction)) {
            $this->arrivalTransactions[] = $arrivalTransaction;
            $arrivalTransaction->setArrivalAccount($this);
        }

        return $this;
    }

    public function removeArrivalTransaction(Transaction $arrivalTransaction): self
    {
        if ($this->arrivalTransactions->contains($arrivalTransaction)) {
            $this->arrivalTransactions->removeElement($arrivalTransaction);
            // set the owning side to null (unless already changed)
            if ($arrivalTransaction->getArrivalAccount() === $this) {
                $arrivalTransaction->setArrivalAccount(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Account[]
     */
    public function getBeneficiaire(): Collection
    {
        return $this->beneficiaire;
    }

    public function addBeneficiaire(Account $beneficiaire): self
    {
        if (!$this->beneficiaire->contains($beneficiaire)) {
            $this->beneficiaire[] = $beneficiaire;
        }

        return $this;
    }

    public function removeBeneficiaire(Account $beneficiaire): self
    {
        if ($this->beneficiaire->contains($beneficiaire)) {
            $this->beneficiaire->removeElement($beneficiaire);
        }

        return $this;
    }
}

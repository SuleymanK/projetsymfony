<?php

namespace App\Controller;

use App\Entity\Transaction;
use App\Form\TransactionCreateType;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\TransactionRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class TransactionController extends AbstractController
{
    /**
     * @Route("/transaction/create", name="create_transaction")
     * @Route("/transaction/{id}/edit", name="edit_transaction")
     */
    public function form(Transaction $transaction = null, Request $request, EntityManagerInterface $em, AuthenticationUtils $authenticationUtils){

        if(!$transaction){
            $transaction = new Transaction();
        }

        $form = $this->createForm(TransactionCreateType::class, $transaction);
        $lastUsername = $authenticationUtils->getLastUsername();
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $transaction = $form->getData();
            $em->persist($transaction);
            $em->flush();

            return $this->redirectToRoute('liste_transaction');
        }

        return $this->render('transaction/create.html.twig',[
            'form' => $form->createView(),
            'editMode' => $transaction->getId() !== null,
            'nomUser' => $lastUsername,
        ]);
    }

    /**
     * @Route("/transaction", name="liste_transaction")
     */
    public function list(TransactionRepository $repo, AuthenticationUtils $authenticationUtils){

        $transactions = $repo->findAll();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render("transaction/index.html.twig", [
            'transactions' => $transactions,
            'nomUser' => $lastUsername,
        ]);
    }

    /**
     * @Route("/transaction/delete/{id}", name="delete_transaction")
     */
    public function delete(Transaction $transaction, EntityManagerInterface $em){

        $em->remove($transaction);
        $em->flush();

        return new Response("Ok delete");
    }
}

<?php

namespace App\Form;

use App\Entity\Account;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BeneficiaireCreateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $account = $builder->getData();
        $builder
            ->add('beneficiaire', EntityType::class, array(
                'class' => Account::class,
                'choice_label' => 'nom',
                'multiple' => true,
                'mapped' => false,
                'query_builder' => function (EntityRepository $er) use ($account){
                    $qb = $er->createQueryBuilder('a')
                    ->where('a != :id')
                    ->setParameter('id', $account);

                    if($account->getBeneficiaire()->count() > 0){
                        $qb->andWhere('a.id NOT IN (:beneficiares)')
                        ->setParameter('beneficiares', $account->getBeneficiaire());
                    }

                    return $qb;
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Account::class,
        ]);
    }
}

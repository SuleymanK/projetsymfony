<?php

namespace App\EventListener;

use App\Entity\Transaction;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class TransactionListener{
    
    public function postPersist(LifecycleEventArgs $args){
        $transaction = $args->getObject();
        $em = $args->getObjectManager();
        if($transaction instanceof Transaction){
            $departureAccount = $transaction->getDepartureAccount();
            $departureAccount->subToBalance($transaction->getAmount());
            $arrivalAaccount = $transaction->getArrivalAccount();
            $arrivalAaccount->addToBalance($transaction->getAmount());

            $em->flush();
        }
    }
}